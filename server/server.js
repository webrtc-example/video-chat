const express = require("express");
const http = require("http");
const app = express();
const server = http.createServer(app);
const socket = require("socket.io");
const io = socket(server);
var userDB = {

}
const rooms = {};


io.on("connection", socket => {
    console.log("connection", socket.id)

    socket.on("message", payload => {
        console.log("message", payload);
        if (!userDB[payload.targetUser]) {
            socket.emit("message", "no User found")
        } else {
            io.to(userDB[payload.targetUser]).emit("message", payload.data);
        }

    });
    socket.on("adduser", payload => {
        console.log("adduser", payload)
        userDB[payload] = socket.id;
        // io.to(payload.target).emit("offer", payload);
    });
    socket.on("sendStream", payload => {
        var userName = Object.keys(userDB).find(key => userDB[key] === socket.id);
        // console.log("streaming", payload)
        if (!userDB[payload.targetUser]) {
            socket.emit("message", "no User found")
        } else {
            io.to(userDB[payload.targetUser]).emit("receiveStream", { data: payload.data, user: userName })
            if (payload.alreadyConnectedTO != payload.targetUser) {
                console.log("sendStreamTO", payload.alreadyConnectedTO, userName)
                io.to(userDB[payload.targetUser]).emit("sendStreamTO", userName);
            }
        }
    });






    socket.on("join room", roomID => {
        if (rooms[roomID]) {
            rooms[roomID].push(socket.id);
        } else {
            rooms[roomID] = [socket.id];
        }
        const otherUser = rooms[roomID].find(id => id !== socket.id);
        if (otherUser) {
            socket.emit("other user", otherUser);
            socket.to(otherUser).emit("user joined", socket.id);
        }
    });

    socket.on("offer", payload => {
        var userName = Object.keys(userDB).find(key => userDB[payload.target] === socket.id);
        console.log("offer",userDB[payload.target],userName);
        io.to(userDB[payload.target]).emit("offer", payload);
    });

    socket.on("answer", payload => {
        var userName = Object.keys(userDB).find(key => userDB[payload.target] === socket.id);
        io.to(userDB[payload.target]).emit("answer", payload);
    });

    socket.on("ice-candidate", incoming => {
        var userName = Object.keys(userDB).find(key => userDB[incoming.target] === socket.id);
        io.to(userDB[incoming.target]).emit("ice-candidate", incoming.candidate);
    });
})


server.listen(8001, () => console.log('server is running on port 8001'));
