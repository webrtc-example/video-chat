import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

function App() {
  return (
    <div>
      <div>
        <Link to="/video-chat" >
          Video Calling without rtcweb
      </Link> (only socket and frames sharing)
      </div>
      <div>
        <Link to="/video-rtc" >
          Video Calling with webrtc
      </Link>
      </div>
      <div>APP COMPONENT</div>
    </div>
  );
}

export default App;
