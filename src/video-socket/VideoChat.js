import React from 'react';
import "./VideoChat.css";
import io from "socket.io-client";


class VideoChat extends React.Component {
    stream;
    recordData = {
        record: null,
        chunks: [],
        download: () => {
            if (this.recordData.chunks.length > 0) {
                var blob = new Blob(this.recordData.chunks, {
                    type: "video/mp4"
                });
                var url = URL.createObjectURL(blob);
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                a.href = url;
                a.download = "test.mp4";
                a.click();
                window.URL.revokeObjectURL(url);
            }
        },
        startRecording: () => {
            var options = { mimeType: "video/webm; codecs=vp9" };
            this.recordData.record = new MediaRecorder(this.stream, options);
            console.log("record", this.recordData);
            this.recordData.record.onstart = () => {
                this.setState({
                    recording: true
                })
            }
            this.recordData.record.onstop = () => {
                this.setState({
                    recording: false
                })
            }
            this.recordData.record.ondataavailable = (media) => { this.recordData.chunks.push(media.data) };
            this.recordData.record.start();
            this.recordData.chunks = []

        },
        stopRecording: () => {
            console.log("record", this.recordData);
            this.recordData.record.stop();
        }
    }
    thumbnailData = {
        thumbnailCanvasTag: null,
        generateThumbnail: () => {
            //generate thumbnail URL data
            var context = this.thumbnailData.thumbnailCanvasTag.current.getContext('2d');
            context.drawImage(this.videoTag.current, 0, 0, 220, 150);
        },
        clearThumbnail: () => {
            var context = this.thumbnailData.thumbnailCanvasTag.current.getContext('2d');
            context.clearRect(0, 0, context.canvas.width, context.canvas.height);
            this.recordData.chunks = [];
            this.forceUpdate();
        }
    }

    socketData = {
        connection: null,
        videoIntervalId: null,
        alreadyConnectedTO: null,
        connectSocket: () => {
            this.socketData.connection = io.connect("localhost:8000");
            console.log("socket", this.socketData.connection);
            // this.socketData.connection.on('connect', function (ele) {
            //     // call the server-side function 'adduser' and send one parameter (value of prompt)
            //     console.log("connet", ele)
            //     this.socketData.connection.emit('adduser', prompt("What's your name?"));
            // });
            this.socketData.connection.on('connect', (x) => {
                this.socketData.connection.emit('adduser', this.state.username);
            });
            this.socketData.connection.on('message', (x) => {
                console.log("message", x)
            });
            this.socketData.connection.on('receiveStream', (x) => {
                console.log("video", x)
                this.socketData.alreadyConnectedTO = x.user
                var context = this.OtherUserVideoTag.current.getContext('2d');
                var img = new Image();
                img.src = x.data;
                img.onload = function () {
                    context.drawImage(img, 0, 0, 220, 150);
                };
            });

            this.socketData.connection.on('sendStreamTO', (x) => {
                console.log("message", x)
                this.setState({ toUser: x })
                this.socketData.sendVideo();
            });

        },
        SendMessage: (tag, data) => {
            this.socketData.connection.emit(tag, {
                data: data,
                targetUser: this.state.toUser
            });
        },
        sendVideo: () => {
            this.socketData.videoIntervalId = setInterval(() => {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext('2d');
                context.drawImage(this.videoTag.current, 0, 0, 220, 150);
                this.socketData.connection.emit("sendStream", {
                    data: canvas.toDataURL('image/webp'),
                    targetUser: this.state.toUser,
                    alreadyConnectedTO: this.socketData.alreadyConnectedTO
                });
            }, 100);
        },
        stopVideoSending: () => {
            clearInterval(this.socketData.videoIntervalId);
        }
    }

    constructor(props) {
        super(props);
        console.log(props);
        this.state = { recording: false, socketMessage: "test", username: "", toUser: "" };
        this.videoTag = React.createRef();
        this.OtherUserVideoTag = React.createRef();
        this.thumbnailData.thumbnailCanvasTag = React.createRef()
    }

    componentDidMount() {
        // getting access to webcam
        navigator.mediaDevices
            .getUserMedia({ video: true, audio: true })
            .then(stream => {
                // this.videoTag.current.src = window.URL.createObjectURL(stream)
                this.videoTag.current.srcObject = stream
                this.stream = stream;
                this.videoTag.current.play()
                console.log("stream", stream, this.stream);

            })
            .catch(console.log);
    }


    updateInput(ele, key) {
        var param = {}
        param[key] = ele.target.value
        this.setState(param)
        console.log(this)
    }

    SocketControlHtml = () => {
        return (<div>
            <div>
                <div>
                    <label>Choose a Username and connect to socket : </label> <input type="text" value={this.state.username} onChange={(event) => this.updateInput(event, "username")} disabled={this.socketData.connection} />
                </div>
                <button onClick={() => { this.socketData.connectSocket(); this.forceUpdate() }}>connect socket</button>
            </div>
            <div>
                <div>
                    <label>Enter Username of user to connect  : </label> <input type="text" value={this.state.toUser} onChange={(event) => this.updateInput(event, "toUser")} />

                    {/* <label>send message : </label> <input type="text" value={this.state.socketMessage} onChange={(event) => this.updateInput(event, "socketMessage")} /> */}
                </div>
                {/* <button onClick={() => this.socketData.SendMessage("message", this.state.socketMessage)}>send message</button> */}
            </div>
            <div>
                <button onClick={() => this.socketData.sendVideo()}>Start Video Call</button>
                <button onClick={() => this.socketData.stopVideoSending()}>stop Video Call</button>
            </div>
            <div>
                <canvas ref={this.OtherUserVideoTag}></canvas>
            </div>
        </div>
        )
    }

    videoControlHtml = (video) => {
        return (
            <div>
                <button onClick={() => { video.current.muted = !video.current.muted; this.forceUpdate() }}>
                    {(video?.current?.muted) ? "Unmuted" : "mute"}
                </button>
                <button onClick={() => this.thumbnailData.generateThumbnail()}>Take Picture</button>
                <button onClick={() => this.recordData.startRecording()}>
                    {(this.state.recording) ? <span className="recording">*</span> : null}
            start Recording
            </button>
                <button onClick={() => this.recordData.stopRecording()}>stop Recording</button>
                <button onClick={() => this.thumbnailData.clearThumbnail()}>Clear</button>
                {(this.recordData.chunks.length > 0) ?
                    <button onClick={() => this.recordData.download()}>download Recording</button>
                    : null}

                <div>
                    <canvas ref={this.thumbnailData.thumbnailCanvasTag}></canvas>
                </div>

            </div>
        )
    }

    render() {
        return (
            <div>
                <h1>This is video Chat </h1>
                <div className="d-flex">
                    <div>
                        <video ref={this.videoTag} > </video>
                    </div>
                    {this.SocketControlHtml()}
                </div>
                {this.videoControlHtml(this.videoTag)}
            </div >
        )
    }
}


export default VideoChat