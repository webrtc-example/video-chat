import React from 'react';
import "./VideoRTC.css"
import io from "socket.io-client";

class VideoRTC extends React.Component {
    streamRtcData = {
        stream: null,
        peerRef: null
    }
    recordData = {
        record: null,
        chunks: [],
        download: () => {
            if (this.recordData.chunks.length > 0) {
                var blob = new Blob(this.recordData.chunks, {
                    type: "video/mp4"
                });
                var url = URL.createObjectURL(blob);
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                a.href = url;
                a.download = "test.mp4";
                a.click();
                window.URL.revokeObjectURL(url);
            }
        },
        startRecording: () => {
            var options = { mimeType: "video/webm; codecs=vp9" };
            this.recordData.record = new MediaRecorder(this.stream, options);
            console.log("record", this.recordData);
            this.recordData.record.onstart = () => {
                this.setState({
                    recording: true
                })
            }
            this.recordData.record.onstop = () => {
                this.setState({
                    recording: false
                })
            }
            this.recordData.record.ondataavailable = (media) => { this.recordData.chunks.push(media.data) };
            this.recordData.record.start();
            this.recordData.chunks = []

        },
        stopRecording: () => {
            console.log("record", this.recordData);
            this.recordData.record.stop();
        }
    }
    thumbnailData = {
        thumbnailCanvasTag: null,
        generateThumbnail: () => {
            //generate thumbnail URL data
            var context = this.thumbnailData.thumbnailCanvasTag.current.getContext('2d');
            context.drawImage(this.videoTag.current, 0, 0, 220, 150);
        },
        clearThumbnail: () => {
            var context = this.thumbnailData.thumbnailCanvasTag.current.getContext('2d');
            context.clearRect(0, 0, context.canvas.width, context.canvas.height);
            this.recordData.chunks = [];
            this.forceUpdate();
        }
    }

    socketData = {
        connection: null,
        videoIntervalId: null,
        alreadyConnectedTO: null,
        connectSocket: () => {
            this.socketData.connection = io.connect("localhost:8001");
            console.log("socket", this.socketData.connection);
            this.socketData.connection.on('connect', (x) => {
                this.socketData.connection.emit('adduser', this.state.username);
            });
            this.socketData.connection.on('message', (x) => {
                console.log("message", x)
            });


            // this.socketData.connection.on("offer", handleRecieveCall);

            // this.socketData.connection.on("answer", handleAnswer);

            // this.socketData.connection.on("ice-candidate", handleNewICECandidateMsg);

            this.socketData.connection.on("offer", this.handleRecieveCall.bind(this));

            this.socketData.connection.on("answer", this.handleAnswer.bind(this));

            this.socketData.connection.on("ice-candidate", this.handleNewICECandidateMsg.bind(this));
        },
        SendMessage: (tag, data) => {
            this.socketData.connection.emit(tag, {
                data: data,
                targetUser: this.state.toUser
            });
        },
        sendVideo: () => {

        },
        stopVideoSending: () => {
        }
    }

    handleRecieveCall(incoming) {
        console.log("handleRecieveCall", incoming, this);
        this.streamRtcData.peerRef = this.createPeer("'hello");
        const desc = new RTCSessionDescription(incoming.sdp);
        this.streamRtcData.peerRef.setRemoteDescription(desc).then(() => {
            this.streamRtcData.stream.getTracks().forEach(track => this.streamRtcData.peerRef.addTrack(track, this.streamRtcData.stream));
        }).then(() => {
            return this.streamRtcData.peerRef.createAnswer();
        }).then(answer => {
            return this.streamRtcData.peerRef.setLocalDescription(answer);
        }).then(() => {
            const payload = {
                target: incoming.caller,
                caller: this.state.username,
                sdp: this.streamRtcData.peerRef.localDescription
            }
            this.socketData.connection.emit("answer", payload);
        })
    }
    handleAnswer(answer) {
        console.log("handleAnswer", answer);
        const desc = new RTCSessionDescription(answer.sdp);
        this.streamRtcData.peerRef.setRemoteDescription(desc).catch(e => console.log(e));
    }
    handleNewICECandidateMsg(d) {
        console.log("handleNewICECandidateMsg", d);
        const candidate = new RTCIceCandidate(d);

        this.streamRtcData.peerRef.addIceCandidate(candidate)
            .catch(e => console.log(e));
    }


    constructor(props) {
        super(props);
        console.log(props);
        this.state = { recording: false, socketMessage: "test", username: "", toUser: "" };
        this.videoTag = React.createRef();
        this.OtherUserVideoTag = React.createRef();
        this.thumbnailData.thumbnailCanvasTag = React.createRef()
    }


    componentDidMount() {
        // getting access to webcam
        navigator.mediaDevices
            .getUserMedia({ video: true, audio: true })
            .then(stream => {
                // this.videoTag.current.src = window.URL.createObjectURL(stream)
                this.videoTag.current.srcObject = stream;
                this.videoTag.current.muted = true;
                this.streamRtcData.stream = stream;
                this.videoTag.current.play()
                console.log("stream", stream, this.stream);

            })
            .catch(console.log);
    }



    callUser(userID) {
        this.streamRtcData.peerRef = this.createPeer(userID);
        this.streamRtcData.stream.getTracks().forEach(track => this.streamRtcData.peerRef.addTrack(track, this.streamRtcData.stream));
        console.log(this.streamRtcData)
    }

    createPeer(userID) {
        const peer = new RTCPeerConnection({
            iceServers: [
                {
                    urls: "stun:stun.stunprotocol.org"
                },
                {
                    urls: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
            ]
        });

        peer.onicecandidate = this.handleICECandidateEvent.bind(this);
        peer.ontrack = this.handleTrackEvent.bind(this);
        peer.onnegotiationneeded = (e) => this.handleNegotiationNeededEvent(e, userID);

        return peer;
    }

    handleICECandidateEvent(e) {
        console.log("handleICECandidateEvent", e)
        if (e.candidate) {
            const payload = {
                target: this.state.toUser,
                candidate: e.candidate,
            }
            this.socketData.connection.emit("ice-candidate", payload);
        }

    }
    handleTrackEvent(e) {
        console.log("handleTrackEvent", e, this.OtherUserVideoTag)
        this.OtherUserVideoTag.current.srcObject = e.streams[0];
        this.OtherUserVideoTag.current.play()


    }
    handleNegotiationNeededEvent(e, userid) {
        console.log("handleNegotiationNeededEvent", e, userid)
        this.streamRtcData.peerRef.createOffer().then(offer => {
            return this.streamRtcData.peerRef.setLocalDescription(offer);
        }).then(() => {
            const payload = {
                target: userid,
                caller: this.state.username,
                sdp: this.streamRtcData.peerRef.localDescription
            };
            console.log("offer", payload);
            this.socketData.connection.emit("offer", payload);
        }).catch(e => console.log(e));
    }

    updateInput(ele, key) {
        var param = {}
        param[key] = ele.target.value
        this.setState(param)
        // console.log(this)
    }



    SocketControlHtml = () => {
        return (<div>
            <div>
                <div>
                    <label>Choose a Username and connect to socket : </label> <input type="text" value={this.state.username} onChange={(event) => this.updateInput(event, "username")} disabled={this.socketData.connection} />
                </div>
                <button onClick={() => { this.socketData.connectSocket(); this.forceUpdate() }}>connect socket</button>
            </div>
            <div>
                <div>
                    <label>Enter Username of user to connect  : </label> <input type="text" value={this.state.toUser} onChange={(event) => this.updateInput(event, "toUser")} />

                    {/* <label>send message : </label> <input type="text" value={this.state.socketMessage} onChange={(event) => this.updateInput(event, "socketMessage")} /> */}
                </div>
                {/* <button onClick={() => this.socketData.SendMessage("message", this.state.socketMessage)}>send message</button> */}
            </div>
            <div>
                <button onClick={() => this.callUser(this.state.toUser)}>callUser</button>
                {/* <button onClick={() => this.socketData.sendVideo()}>Start Video Call</button> */}
                {/* <button onClick={() => this.socketData.stopVideoSending()}>stop Video Call</button> */}
            </div>
            <div>
                <video ref={this.OtherUserVideoTag} className="othervideo"></video>
            </div>
        </div>
        )
    }

    videoControlHtml = (video) => {
        return (
            <div>
                <button onClick={() => { video.current.muted = !video.current.muted; this.forceUpdate() }}>
                    {(video?.current?.muted) ? "Unmuted" : "mute"}
                </button>
                <button onClick={() => this.thumbnailData.generateThumbnail()}>Take Picture</button>
                <button onClick={() => this.recordData.startRecording()}>
                    {(this.state.recording) ? <span className="recording">*</span> : null}
            start Recording
            </button>
                <button onClick={() => this.recordData.stopRecording()}>stop Recording</button>
                <button onClick={() => this.thumbnailData.clearThumbnail()}>Clear</button>
                {(this.recordData.chunks.length > 0) ?
                    <button onClick={() => this.recordData.download()}>download Recording</button>
                    : null}

                <div>
                    <canvas ref={this.thumbnailData.thumbnailCanvasTag}></canvas>
                </div>

            </div>
        )
    }
    render() {
        return (
            <div>
                <h1>This is video Chat </h1>
                <div className="d-flex">
                    <div>
                        <video ref={this.videoTag} > </video>
                    </div>
                    {this.SocketControlHtml()}
                </div>
                {this.videoControlHtml(this.videoTag)}
            </div >
        )
    }
}

export default VideoRTC
