This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify




### simple video sharing demo using node server
- npm run client
- npm run server

# client
- view the client page in localhost/video-chat
- client shows video from webcam with all the controls(mute,record,etc) 
- there is a option to connect to socket and share video with other user

# socket working
- step 1 - user enters the user name and connect to socket
- step 2 - enter username to whom we need to share video and click sendVideo

# video sharing using socket without rtcweb
- webcam video stream is displayed using video tag
- socket connection is established with server
- sender captures the frames in the video and sends the frame(image) to receiver using socket
- receiver on getting the frame from sender displays it in canvas
- setinterval is used to continuously to send the frame 

# video sharing using socket and rtcweb
- sender connects to stun server(RTCPeerConnection)
- Negotiation happens
- sender creates an offer
- sender sends the offer(contains RTCSessionDescription) to receiver through socket
- to create an answer receiver connects to stun server(RTCPeerConnection)
- receiver creates an answer using RTCSessionDescription(senders session)
- receiver sends the answer(contains RTCSessionDescription) to sender
- sender register the answer(RTCSessionDescription) received from receiver

- meanwhile ICECandidate(events of RTCPeerConnection) is initiated on sender
- In ICECandidate event sender gets the candidate details and sends that to receiver
- receiver adds that candidate details to its RTCPeerConnection

- meanwhile receiver receives the TrackEvent from RTCPeerConnection which contains sender streams
- meanwhile sender receives the TrackEvent from RTCPeerConnection which contains receiver streams

-> Negotiation -> offer -> answer -> icecandidate -> handleTrackEvent
